/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"strings"
	"time"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	sitedetailsv1alpha1 "gitlab.cern.ch/paas-tools/operators/sitedetails-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

var CreationDateAnnotationKey string

// HostnameInfoReconciler reconciles *namespaces* and maintains the HostnameInfo resources associated with that namespace
type HostnameInfoReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	// A label selector that matches the namespaces this controller should process.
	// It only makes sense to process "user namespaces" that have been registered in the Application Portal by the authz-operator.
	NamespaceSelector labels.Selector
	// Definitions of all the hostname-defining resources we should take into account to find the hostnames used in a namespace.
	ResourcesWithHostnames []ResourceWithHostnames
}

// Describes a Kubernetes resource that defines one or more hostnames.
// Such resources are the source for HostnameInfo.
// Examples: built-in OKD routes, all the custom operator-backed resources for Drupal, Webeos, App-catalogue...
type ResourceWithHostnames struct {
	// a type to show in the webservices portal for this hostname (e.g. PaaS, Wordpress, Drupal...)
	DisplayType string
	// defines the Group, Version, Kind fields
	schema.GroupVersionKind
	// "path" to walk in the resource to get a hostname or an array of hostnames.
	// E.g. for a route the host is at spec.host so the path is ["spec", "host"].
	// This is similar to rego's object.get https://www.openpolicyagent.org/docs/latest/policy-reference/#builtin-object-objectget
	// and we want this similarity to implement hostname unicity rules with OPA in a generic way, from the same list of ResourceWithHostnames used by this operator.
	// The value found at that path must be either a string (single hostname) or an array of strings (multiple hostnames)
	// NB: this is sufficient for OKD routes and operator-backed custom resources but NOT appropriate for an Ingress resource
	// that can have a list of rules, each with a hostname. To support such resources we'd need a second array "PathToRules" to get to the array of rules,
	// and then we'd apply the PathToHostnames to each rule (e.g. PathToRules=["spec", "rules"] and PathToHostnames=["host"] for an Ingress resource).
	// However this is not needed as of June 2022.
	PathToHostnames []string
}

//+kubebuilder:rbac:groups=sitedetails.webservices.cern.ch,resources=hostnameinfos,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
//
// In each reconciliation we look at all HostnameInfo resources in the namespace and update them.
// So we're actually reconciling **namespaces**, not the HostnameInfo resources.
//
// Garbage collection considerations:
// We do nothing when a namespace is deleting/deleted, in particular we will not explicitly delete the HostnameInfo
// resources when a namespace is deleted; instead we rely entirely on setting the Namespace as the owner of the
// HostnameInfo (with blockOwnerDeletion=true), so that Kubernetes cascading deletion will delete the HostnameInfos.
// (see https://kubernetes.io/docs/concepts/architecture/garbage-collection/)
// This allows HostnameInfo resources to be GC'ed even if the operator is not running, and without
// adding a dedicated finalizer on the namespace.
// NB: like for all owner/dependent relationship in Kubernetes this creates the possibility for a namespace to be
// deleted and explicitly leave orphaned HostnameInfo resources with
// https://kubernetes.io/docs/tasks/administer-cluster/use-cascading-deletion/#set-orphan-deletion-policy
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.0/pkg/reconcile
func (r *HostnameInfoReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx, "namespace", req.NamespacedName)

	// debug
	logger.V(5).Info("Starting reconciliation")

	// We reconcile namespaces. At every reconcile, we update all HostnameInfo resources in the namespace.
	namespace := corev1.Namespace{}
	if err := r.Client.Get(ctx, req.NamespacedName, &namespace); err != nil {
		if apierrors.IsNotFound(err) {
			// namespace doesn't exist, nothing to do
			// see description of Reconcile function above regarding garbage collection of HostnameInfo owned by the deleted namespace
			return ctrl.Result{}, nil
		} else {
			return ctrl.Result{}, err
		}
	}

	// Do nothing if namespace is terminating to avoid a race condition between Kubernetes cascading deletion and this operator
	// re-creating HostnameInfo resources - this could result in orphan HostnameInfo resources.
	// See description of Reconcile function above regarding garbage collection of HostnameInfo owned by the deleted namespace
	if namespace.GetDeletionTimestamp() != nil {
		return ctrl.Result{}, nil
	}

	// List existing HostnameInfo associate with the namespace.
	existingHostnameInfoMap, err := r.getHostnameInfoMapForNamespace(ctx, &namespace)
	if err != nil && !apierrors.IsNotFound(err) {
		return ctrl.Result{}, err
	}

	// safeguard against multiple resources in a namespace using the same hostname. Only the first hostname will generate a HostnameInfo.
	alreadyProcessedHostnames := map[string]bool{}
	// safeguard against multiple namespaces owning the same hostname. If another namespace is found to own a hostname we think we should own,
	// wait for the other namespace to release it and retry after a while.
	anotherNamespaceOwnsOurHostname := false

	// Only consider resources in user namespaces - check namespace against a configurable label selector https://medium.com/coding-kubernetes/using-k8s-label-selectors-in-go-the-right-way-733cde7e8630
	// Note that in case it's _not_ a user namespace we've still retrieved the existing HostnameInfo for the non-user namespace. This allows applying the cleanup logic and delete any existing HostnameInfo
	// associated with the non-user namespace.
	if r.NamespaceSelector.Matches(labels.Set(namespace.Labels)) {

		// look for all types of resources that define hostnames in the namespace and create/update the corresponding HostnameInfo
		for _, resourceKind := range r.ResourcesWithHostnames {
			// a resource may define multiple hostnames
			hostnames := r.getHostnames(ctx, &namespace, resourceKind)
			// Here we need each hostname to be actually valid hostnames. It is assumed this will be enforced by other means (CRD schema, OPA policy...).
			// Reconciliation will fail if a hostname is not a valid value.
			// We leverage the property that Kubernetes resource names are DNS subdomains (https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-subdomain-names)
			// i.e. we assign the hostname to the HostnameInfo resource's name. This will be valid if and only if the hostname is a valid DNS hostname.
			for _, h := range hostnames {
				if alreadyProcessedHostnames[h] {
					logger.V(6).Info("Already processed this hostname in this namespace", "hostname", h)
					continue
				}

				alreadyProcessedHostnames[h] = true
				hi := sitedetailsv1alpha1.HostnameInfo{
					ObjectMeta: metav1.ObjectMeta{
						Name: h,
					},
				}

				// CreateOrUpdate will ensure the HostnameInfo resource exists with the expected content.
				// It is idempotent so can be called multiple times with the same hostname as long as it's for the same namespace and resourceKind.
				if op, err := controllerutil.CreateOrUpdate(ctx, r.Client, &hi, func() error {
					if currentController := metav1.GetControllerOf(&hi.ObjectMeta); currentController != nil && currentController.Kind == namespace.Kind && currentController.Name != namespace.Name {
						// The hostnameInfo for this hostname is already owned by *another* namespace.
						// If both current namespace and that other namespace think they should own it, we'll endlessly transfer ownership between them.
						// So wait for the other namespace to release this HostnameInfo first.
						logger.V(6).Info("Another namespace already owns this hostname", "hostname", h, "otherNamespace", currentController.Name)
						anotherNamespaceOwnsOurHostname = true
						return nil
					}

					logger.V(5).Info("Updating HostnameInfo", "hostname", h, "displayType", resourceKind.DisplayType)

					hi.Data.DisplayType = resourceKind.DisplayType
					hi.Data.ProjectName = namespace.Name
					fillHostnameInfoDataFromNamespaceLifecycleAnnotations(&hi, &namespace)
					fillHostnameInfoDataCreationTimestamp(&hi, &namespace, logger)
					// set the Namespace as the owner of the HostnameInfo resource. This serves 2 purposes:
					// - garbage collection of the HostnameInfo when associated project/namespace is deleted
					// - know which namespace to reconcile to ensure a HostnameInfo is up to date (see Owns() in SetupWithManager())
					return controllerutil.SetControllerReference(&namespace, &hi, r.Scheme)
				}); err != nil {
					return ctrl.Result{}, err
				} else {
					logger.V(6).Info("CreateOrUpdate returned", "result", op)
				}

				// remove the HostnameInfo from existingHostnameInfoMap. If after this loop there's still some entries in existingHostnameInfoMap this will mean
				// there is no resource in the namespace related to that HostnameInfo, and it should be deleted from the namespace.
				delete(existingHostnameInfoMap, h)
			}

		}
	} else {
		// debug
		logger.V(5).Info("Ignored namespace because it doesn't match selector", "selector", r.NamespaceSelector.String(), "labels", namespace.Labels)
	}

	// Any existing HostnameInfo still in existingHostnameInfoMap at this point needs to be deleted from the cluster because there isn't a resource in the namespace
	// declaring that name anymore. (It could also be HostnameInfo resources owned by a non-user namespace, these should also be deleted)
	for _, existingHostnameInfo := range existingHostnameInfoMap {
		logger.V(6).Info("Deleting extraneous HostnameInfo owned by namespace", "hostname", existingHostnameInfo.Name)
		if err := r.Client.Delete(ctx, &existingHostnameInfo); err != nil {
			return ctrl.Result{}, err
		}
	}

	// requeue if another namespace already owns a HostnameInfo we want to own, in case it will release it.
	// If it never happens then exponential back-off means we'll retry less and less often
	return ctrl.Result{Requeue: anotherNamespaceOwnsOurHostname}, nil
}

// Finds the existing HostnameInfo resources associated with a namespace.
// Return a map hostname -> the HostnameInfo resource
func (r *HostnameInfoReconciler) getHostnameInfoMapForNamespace(ctx context.Context, namespace *corev1.Namespace) (map[string]sitedetailsv1alpha1.HostnameInfo, error) {
	// For efficiency we ask the operator-sdk to index HostnameInfo by a custom (virtual) field ownerNamespace (see main.go for setting up this Index)
	// Ref: https://stackoverflow.com/questions/57083221/list-custom-resources-from-caching-client-with-custom-fieldselector
	listOptions := client.ListOptions{
		FieldSelector: fields.SelectorFromSet(fields.Set{"ownerNamespace": namespace.Name}),
	}

	// use a map to keep track of which ones are still relevant
	existingHostnameInfoMap := make(map[string]sitedetailsv1alpha1.HostnameInfo)

	existingHostnameInfoList := sitedetailsv1alpha1.HostnameInfoList{}
	if err := r.List(ctx, &existingHostnameInfoList, &listOptions); err != nil && !apierrors.IsNotFound(err) {
		return existingHostnameInfoMap, err
	}
	for _, hi := range existingHostnameInfoList.Items {
		existingHostnameInfoMap[hi.Name] = hi
	}

	return existingHostnameInfoMap, nil
}

// For a given resource type defining hostnames (e.g. a route or a DrupalSite), find all resources in the namespace and returns the aggregated list of hostnames.
// A same hostname can be repeated in the result (e.g. routes with paths).
// hostnames are lowercases to account for DNS case-insensitivity
func (r *HostnameInfoReconciler) getHostnames(ctx context.Context, namespace *corev1.Namespace, resource ResourceWithHostnames) []string {
	hostnames := []string{}

	// we need to handle unstructured data because we don't have the full resource type definitions - only the Group/Version/Kind
	// and JSON path to the hostnames in the resource's JSON representation.
	// References:
	// https://github.com/operator-framework/operator-sdk/blob/65480f48d1ef3276e0bd1f85da594a858c943931/internal/helm/controller/controller.go#L147
	// https://golangdocs.com/json-with-golang
	// https://golangdocs.com/type-assertions-in-golang

	resourceList := unstructured.UnstructuredList{}
	resourceList.SetGroupVersionKind(resource.GroupVersionKind)
	r.Client.List(ctx, &resourceList, &client.ListOptions{Namespace: namespace.Name})

	for _, r := range resourceList.Items {
		var element interface{}
		obj := r.Object
		lastIndex := len(resource.PathToHostnames) - 1
		// walk the PathToHostname in the unstructured data
		for pathIndex, pathKey := range resource.PathToHostnames {
			element = obj[pathKey]
			// if we're at the last index then we're done: element is the hostname or array of hostnames.
			// Otherwise we need to keep walking the path, but we can only do so if the element is a map[string]interface{}.
			// If it's not the case then abort.
			if pathIndex < lastIndex {
				var ok bool
				if obj, ok = element.(map[string]interface{}); !ok {
					element = nil
					break
				}
			}
		}
		// we can extract hostnames if element is a string (single hostname) or an array of strings (multiple hostnames)
		if singleHostname, ok := element.(string); ok {
			hostnames = append(hostnames, strings.ToLower(singleHostname))
		} else if multiHostnames, ok := element.([]interface{}); ok {
			for _, h := range multiHostnames {
				if oneHostname, ok := h.(string); ok {
					hostnames = append(hostnames, strings.ToLower(oneHostname))
				}
			}
		}
	}

	return hostnames
}

// authz-operator's lifecycle controller sets the following labels on user namespaces:
//    lifecycle.webservices.cern.ch/adminGroup: my-egroup
//    lifecycle.webservices.cern.ch/owner: alossent
//    lifecycle.webservices.cern.ch/resourceCategory: Test
// as well as the standard OKD project annotation for description `openshift.io/description`
func fillHostnameInfoDataFromNamespaceLifecycleAnnotations(hi *sitedetailsv1alpha1.HostnameInfo, namespace *corev1.Namespace) {
	hi.Data.AdministratorGroup = namespace.Labels["lifecycle.webservices.cern.ch/adminGroup"]
	hi.Data.Category = namespace.Labels["lifecycle.webservices.cern.ch/resourceCategory"]
	hi.Data.Owner = namespace.Labels["lifecycle.webservices.cern.ch/owner"]
	hi.Data.Description = namespace.Annotations["openshift.io/description"]
}

// Migrated projects have "cern.ch/webservices-creation-date" annotation with the original creation date.
// Otherwise, we are using the namespace creation date to fill the CreationTimestamp on a HostnameInfo.
func fillHostnameInfoDataCreationTimestamp(hi *sitedetailsv1alpha1.HostnameInfo, namespace *corev1.Namespace, logger logr.Logger) {
	// By default, use the namespace creation date
	hi.Data.CreationTimestamp = namespace.CreationTimestamp

	// Check if the namespace has been migrated from old webservices
	annotationValue := namespace.Annotations[CreationDateAnnotationKey]
	if annotationValue != "" {
		t, err := time.Parse(time.RFC3339, annotationValue)
		if err != nil {
			logger.Error(err, "Unable to parse namespace annotation '%s=%s' as RFC3339: %s", CreationDateAnnotationKey, annotationValue)
			return
		}
		// Found a valid namespace annotation, using it as the creation timestamp
		hi.Data.CreationTimestamp = metav1.Time{Time: t}
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *HostnameInfoReconciler) SetupWithManager(mgr ctrl.Manager) error {
	// when there's an event on a resource we watch, we want to reconcile its namespace
	reconcileParentNamespace := func(a client.Object) []reconcile.Request {
		req := reconcile.Request{}
		req.Name = a.GetNamespace()
		return []reconcile.Request{req}
	}

	controller := ctrl.NewControllerManagedBy(mgr).
		// reconcile namespaces if they change (e.g. update of the lifecycle-related annotations)
		For(&corev1.Namespace{}).
		// reconcile on changes to HostnameInfo (ensures they are as expected).
		// HostnameInfo resources are cluster-scoped but will be owned by the Namespace where the hostname is defined.
		// This will reconcile the owning namespace when there's a change on a HostnameInfo.
		Owns(&sitedetailsv1alpha1.HostnameInfo{})

	// Add generic watches for each hostname-defining resources - ref https://github.com/operator-framework/operator-sdk/blob/65480f48d1ef3276e0bd1f85da594a858c943931/internal/helm/controller/controller.go#L147
	for _, resourceKind := range r.ResourcesWithHostnames {
		apiType := &unstructured.Unstructured{}
		apiType.SetGroupVersionKind(resourceKind.GroupVersionKind)
		controller = controller.Watches(&source.Kind{Type: apiType}, handler.EnqueueRequestsFromMapFunc(reconcileParentNamespace))
		// log something because default manager logs won't show the actual resource kind being watched, only generic unstructured.Unstructured
		log.Log.Info("Watching hostname-providing resource", "gvk", resourceKind.GroupVersionKind, "displayType", resourceKind.DisplayType)
	}

	return controller.Complete(r)
}
