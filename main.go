/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"flag"
	"os"
	"time"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	"go.uber.org/zap/zapcore"
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	zaplogfmt "github.com/sykesm/zap-logfmt"
	sitedetailsv1alpha1 "gitlab.cern.ch/paas-tools/operators/sitedetails-operator/api/v1alpha1"
	"gitlab.cern.ch/paas-tools/operators/sitedetails-operator/controllers"
	uzap "go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	//+kubebuilder:scaffold:imports
)

var (
	err                     error
	configFile              string
	leaderElectionNamespace string
	scheme                  = runtime.NewScheme()
	setupLog                = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(sitedetailsv1alpha1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	flag.StringVar(&configFile, "config", "./config/manager/controller_manager_config.yaml",
		"The controller will load its initial configuration from this file. "+
			"Omit this flag to use the default configuration values. "+
			"Command-line flags override configuration from this file.")
	flag.StringVar(&leaderElectionNamespace, "leader-election-namespace", "",
		"Namespace used to perform leader election. Only needed when not running in-cluster. "+
			"(default \"\", i.e. use pod's namespace when running in-cluster)")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	// Format logs timestamp to time.RFC3339Nano
	// Reference: https://sdk.operatorframework.io/docs/building-operators/golang/references/logging/#custom-zap-logger
	configLog := uzap.NewProductionEncoderConfig()
	configLog.EncodeTime = func(ts time.Time, encoder zapcore.PrimitiveArrayEncoder) {
		encoder.AppendString(ts.UTC().Format(time.RFC3339Nano))
	}
	logfmtEncoder := zaplogfmt.NewEncoder(configLog)
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts), zap.Encoder(logfmtEncoder)))

	options := ctrl.Options{
		Scheme:                  scheme,
		LeaderElectionNamespace: leaderElectionNamespace}
	config := sitedetailsv1alpha1.Config{}

	if configFile != "" {
		options, err = options.AndFrom(ctrl.ConfigFile().AtPath(configFile).OfKind(&config))
		if err != nil {
			setupLog.Error(err, "unable to load the config file")
			os.Exit(1)
		}
	}
	controllers.CreationDateAnnotationKey = config.CreationDateAnnotationKey

	parsedNamespaceSelector, err := labels.Parse(config.NamespaceSelector)
	if err != nil {
		setupLog.Error(err, "unable to parse namespace selector")
		os.Exit(1)
	}

	resourcesWithHostnames := []controllers.ResourceWithHostnames{}
	for _, resource := range config.Resources {
		resourcesWithHostnames = append(resourcesWithHostnames, controllers.ResourceWithHostnames{
			DisplayType: resource.DisplayType,
			GroupVersionKind: schema.GroupVersionKind{
				Group:   resource.Group,
				Version: resource.Version,
				Kind:    resource.Kind,
			},
			PathToHostnames: resource.PathToHostnames,
		})

	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), options)
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	// For performance of the HostnameInfo controller it is critical to efficiently find the HostnameInfo associated with a namespace.
	// Add a custom index on virtual field "ownerNamespace"
	// Ref: https://stackoverflow.com/questions/57083221/list-custom-resources-from-caching-client-with-custom-fieldselector
	indexByNamespaceControllerOwnerFunc := func(obj client.Object) []string {
		controllerOwner := metav1.GetControllerOf(obj)
		if controllerOwner != nil && controllerOwner.Kind == "Namespace" {
			return []string{controllerOwner.Name}
		}
		return []string{}
	}
	if err := mgr.GetCache().IndexField(context.Background(), &sitedetailsv1alpha1.HostnameInfo{}, "ownerNamespace", indexByNamespaceControllerOwnerFunc); err != nil {
		setupLog.Error(err, "unable to create custom index on HostnameInfo cache")
		os.Exit(1)
	}

	if err = (&controllers.HostnameInfoReconciler{
		Client:                 mgr.GetClient(),
		Scheme:                 mgr.GetScheme(),
		NamespaceSelector:      parsedNamespaceSelector,
		ResourcesWithHostnames: resourcesWithHostnames,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "HostnameInfo")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
