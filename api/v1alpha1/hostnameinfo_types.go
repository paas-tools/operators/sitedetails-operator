/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Important: Run "make" to regenerate code after modifying this file

// Contains information about a hostname.
type HostnameInfoData struct {
	// The username of the owner of the parent project for that hostname
	Owner string `json:"owner,omitempty"`
	// The administrator group of the parent project for that hostname
	AdministratorGroup string `json:"administratorGroup,omitempty"`
	// The description of the parent project for that hostname
	Description string `json:"description,omitempty"`
	// The category of the parent project for that hostname (Official, Personal or Test)
	Category string `json:"category,omitempty"`
	// Name of the parent project for that hostname
	ProjectName string `json:"projectName,omitempty"`
	// The display type for the webservices portal to show what kind of web site or application is served at that hostname (e.g. PaaS, Wordpress etc.)
	DisplayType string `json:"displayType,omitempty"`
	// The creation timestamp of the project
	CreationTimestamp metav1.Time `json:"creationTimestamp,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:resource:path=hostnameinfos,scope=Cluster
//+kubebuilder:printcolumn:name="Project",type=string,JSONPath=`.data.projectName`
//+kubebuilder:printcolumn:name="Owner",type=string,JSONPath=`.data.owner`
//+kubebuilder:printcolumn:name="Admin Group",type=string,JSONPath=`.data.administratorGroup`
//+kubebuilder:printcolumn:name="Category",type=string,JSONPath=`.data.category`
//+kubebuilder:printcolumn:name="Project Age",type=date,JSONPath=`.data.creationTimestamp`
//+kubebuilder:printcolumn:name="Description",type=string,JSONPath=`.data.description`
// HostnameInfo provides information about a hostname exposed by the cluster. The name of the resource is the hostname whose details are provided.
// HostnameInfo is a cluster-scope resource so one can query for the details of a hostname without prior knowledge of its parent namespace.
type HostnameInfo struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Data HostnameInfoData `json:"data,omitempty"`
}

//+kubebuilder:object:root=true

// HostnameInfoList contains a list of HostnameInfo
type HostnameInfoList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []HostnameInfo `json:"items"`
}

func init() {
	SchemeBuilder.Register(&HostnameInfo{}, &HostnameInfoList{})
}
