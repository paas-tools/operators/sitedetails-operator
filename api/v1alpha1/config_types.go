/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	cfg "sigs.k8s.io/controller-runtime/pkg/config/v1alpha1"
)

// Resource defines a resource type
type Resource struct {
	// The display type for the webservices portal to show what kind of web site or application is served at that hostname (e.g. PaaS, Wordpress etc.)
	// +kubebuilder:validation:Required
	DisplayType string `json:"displayType"`
	// The group of the resource
	// +kubebuilder:validation:Required
	Group string `json:"group"`
	// The version of the resource
	// +kubebuilder:validation:Required
	Version string `json:"version"`
	// The kind of the resource
	// +kubebuilder:validation:Required
	Kind string `json:"kind"`
	// The path in the resource to get a hostname or an array of hostnames
	// +kubebuilder:validation:Required
	PathToHostnames []string `json:"pathToHostnames"`
}

//+kubebuilder:object:root=true

// Config is the Schema for the configs API
type Config struct {
	metav1.TypeMeta `json:",inline"`

	// ControllerManagerConfigurationSpec returns the contfigurations for controllers
	cfg.ControllerManagerConfigurationSpec `json:",inline"`

	// A label selector that matches the namespaces the controller should process.
	// It only makes sense to process user namespaces.
	NamespaceSelector string `json:"namespaceSelector"`
	// Annotation for creation date of migrated sites
	CreationDateAnnotationKey string     `json:"creationDateAnnotationKey"`
	Resources                 []Resource `json:"resources"`
}

func init() {
	SchemeBuilder.Register(&Config{})
}
