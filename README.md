# sitedetails operator

This operator generates resources with high-level information about web sites hosted in the cluster, which can be used by the webservices portal for search functionality.

## HostnameInfo resource

This resource is needed by the [webservices portal](https://gitlab.cern.ch/webservices/webservices-portal) to implement search by hostnames.

The operator consumes:

- the various resources defining what hostnames are used by the applications/web sites hosted in the cluster: `routes` for the PaaS use case,
  and the various operator-backed custom resources defining web sites and applications in the app-catalogue, webeos, drupal use-cases.
- the lifecycle metadata set by the [authz-operator](https://gitlab.cern.ch/paas-tools/operators/authz-operator) as annotations on user namespaces,
  providing information about ownership, category etc of the parent project

And generates `HostnameInfo` resources looking like:

```yaml
apiVersion: sitedetails.webservices.cern.ch/v1alpha1
kind: HostnameInfo
metadata:
  name: myhostname.web.cernch
spec:
  owner: myusername
  administratorGroup: myadmingroup
  description: my application
  category: Test
  displayType: PaaS
  projectName: myproject
```

This is implemented by the `HostnameInfoReconciler` controller that watches both namespaces and the various hostname-defining resources (routes + custom resources)
and updates the `HostnameInfo` resources in the namespace.

Note that this is quite different from a typical operator reconciling `HostnameInfo` custom resources, since in this case the `HostnameInfo` resource is the _output_
of the operator's reconciliation logic.

## Development

Needs `operator-sdk`

Run locally with verbose logs:

```bash
make build
KUBECONFIG=.../cluster_artifacts/mycluster/auth/kubeconfig bin/manager -leader-election-namespace openshift-cern-sitedetails-operator -zap-log-level=6 -config ./config/manager/controller_manager_config.yaml
```

This will use the default configuration file at `config/manager/controller_manager_config.yaml`
for the PaaS use case (`route` resources).
To test other use cases/resources, create and use another configuration file.